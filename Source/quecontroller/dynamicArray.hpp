//
//  dynamicArray.hpp
//  CommandLineTool
//
//  Created by Joseph D'Souza on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//
//
#ifndef dynamicArray_hpp
#define dynamicArray_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../Source/ui/AudioQuePlayerUI.h"

class Array //: public Component
{
public:
    Array();
    ~Array();
    void add (AudioQuePlayerUI itemValue);
    void remove (int index);
    AudioQuePlayerUI get (int index);
    int size();
    
private:
    int iAsize;
    AudioQuePlayerUI* aPointer = nullptr;
};

#endif /* dynamicArray_hpp */
