//==============================================================================
//  dynamicarray.c
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#include "dynamicarray.h"

//create dynamic memory.
Array::Array()
{
    aPointer = new int[0];
    iAsize = 0;
}

//delete dynamic memory.
Array::~Array()
{
    delete[] aPointer;
    aPointer = nullptr;
}

void Array::add (int itemValue)
{
    //memcpy more efficient.
    
    //create temporary pointer and new array.
    int* aPointerTemp = nullptr;
    aPointerTemp = new int[Array::iAsize + 1];
    
    //copy old array values to new array.
    for(int i = 0; i <= Array::iAsize; i++)
    {
        aPointerTemp[i] = aPointer[i];
    }
    
    //increment array index, input new value to array.
    Array::iAsize++;
    aPointerTemp[Array::iAsize -1 ] = itemValue;
    
    //delete old array and assign new array to class pointer(for the array).
    delete[] aPointer;
    aPointer = aPointerTemp;
}

void Array::remove (int index)
{
    //memcpy more efficient.
    
    //create temporary pointer and new array.
    int i[2] = {0,0};
    int* aPointerTemp = nullptr;
    aPointerTemp = new int[Array::iAsize -1];
    
    //copy old array values to new array. Skipping unwanted value.
    for(i[0] = 0; i[0] <= Array::iAsize; i[0]++, i[1]++)
    {
        if(i[0] != index){
            aPointerTemp[i[0]] = aPointer[i[1]];
        }
        else{
            i[1]++;
            aPointerTemp[i[0]] = aPointer[i[1]];
        }
        
    }
    
    //decrement array index.
    Array::iAsize--;
    
    //delete old array and assign new array to class pointer(for the array).
    delete[] aPointer;
    aPointer = aPointerTemp;
}

int Array::get (int index)
{
    //return requested array value.
    return Array::aPointer[index];
}

int Array::size()
{
    //return array index value.
    return Array::iAsize;
    //return 0;
}


