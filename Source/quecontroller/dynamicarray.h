/*==============================================================================
//  dynamicarray.h
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

#include "../../JuceLibraryCode/JuceHeader.h"
//#include "../Source/ui/AudioQuePlayerUI.h"
#include <stdio.h>
#include <iostream>

class Array
{
public:
    Array();
    ~Array();
    
    void add (int itemValue);
    void remove (int index);
    int get (int index);
    
    int size();
    
private:
    int iAsize;
    int* aPointer = nullptr;
};

#endif /* dynamicarray_h */
