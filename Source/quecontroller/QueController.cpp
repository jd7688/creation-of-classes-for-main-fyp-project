//==============================================================================
//  QueController.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#include "QueController.hpp"

//mainstuff=====================================================================
QueController::QueController(Audio& audio) : audio (audio)
{
    queIndex = 0;
    playbackIndex = 0;
    audioQuePlayerUIArrayIndex = 0;
    audioQuePlayerUIArray = new AudioQuePlayerUI[0];
}

QueController::~QueController()
{
    delete audioQuePlayerUIArray;
}

//playcues======================================================================
void QueController::playNextCue()
{
    DBG("Play Next Cues");
 //   audio.addAudioSource(<#AudioQuePlayer *newAudioSource#>);
}

void QueController::stopAllCues()
{
    DBG("Stop All Cues");
}

void QueController::fadeAllCues()
{
    DBG("Fade All Cues");
    stopAllCues();                                   //Call after fade complete.
}

//AudioQueFunctions=============================================================
AudioQuePlayerUI* QueController::newAudioQue()
{
    DBG("NewAudioCue");
    AudioQuePlayerUI* audioQuePlayerUI = new AudioQuePlayerUI();
    audioQuePlayerUI->setBounds(80, (queIndex * 20) + 5, 420, 20);
    incQueIndex();
    return audioQuePlayerUI;
}

void QueController::incArraySize(AudioQuePlayerUI* newAudioQuePlayerUI)
{
    int index = 0;
    
    AudioQuePlayerUI* tempArray = nullptr;
    tempArray = new AudioQuePlayerUI[audioQuePlayerUIArrayIndex + 1];
    
    for (index=0; index <= audioQuePlayerUIArrayIndex; index++)
    {
       tempArray[index] = audioQuePlayerUIArray[index];
    }
    
    audioQuePlayerUIArrayIndex++;
    index++;
    
    delete[] audioQuePlayerUIArray;
    audioQuePlayerUIArray = tempArray;
    
    audioQuePlayerUIArray[index] = *newAudioQuePlayerUI;
}

void QueController::removeAudioQue()
{
    
}

AudioQuePlayerUI* QueController::get (int index)
{
    
}

int QueController::size()
{
    
}













