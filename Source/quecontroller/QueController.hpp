<<<<<<< HEAD
//
//  QueController.hpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//
//

#ifndef QueController_hpp
#define QueController_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/AudioQuePlayer.h"
#include "../audio/Audio.h"
#include "../ui/AudioQuePlayerUI.h"
#include "dynamicArray.hpp"


class QueController :   public Component
                        //public MenuBarModel,
                        //public ButtonListener
{
public:
    //mainstuff=========================================
    QueController(Audio& audio);
    ~QueController();
    
    //queIndex Controls==================================
    int getQueIndex(){return queIndex;}
    void setQueIndex(int newIndex){queIndex = newIndex;}
    void incQueIndex(){queIndex++;}
    
    //queIndex Controls==================================
    int getPlaybackIndex(){return playbackIndex;}
    void setPlaybackIndex(int newIndex){playbackIndex = newIndex;}
    void incPlaybackIndex(){playbackIndex++;}
    
    //playcues===========================================
    void playNextCue();
    void stopAllCues();
    void fadeAllCues();
    
    //AudioQueFunctions==================================
    AudioQuePlayerUI* newAudioQue();
    void incArraySize(AudioQuePlayerUI* newAudioQuePlayerUI);
    
private:
    int queIndex;
    int playbackIndex;
    int arraySize;
    AudioQuePlayerUI* audioQuePlayerUIArray;
//    Array dynamicArray;
    
    Audio& audio;
    
};

#endif /* QueController_hpp */
=======
//==============================================================================
//  QueController.hpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================

#ifndef QueController_hpp
#define QueController_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/AudioQuePlayer.h"
#include "../audio/Audio.h"
#include "../ui/AudioQuePlayerUI.h"

class QueController :   public Component
                        //public MenuBarModel,
                        //public ButtonListener
{
public:
    //mainstuff=========================================
    QueController(Audio& audio);
    ~QueController();
    
    //queIndex Controls==================================
    int getQueIndex(){return queIndex;}
    void setQueIndex(int newIndex){queIndex = newIndex;}
    void incQueIndex(){queIndex++;}
    
    //queIndex Controls==================================
    int getPlaybackIndex(){return playbackIndex;}
    void setPlaybackIndex(int newIndex){playbackIndex = newIndex;}
    void incPlaybackIndex(){playbackIndex++;}
    
    //playcues===========================================
    void playNextCue();
    void stopAllCues();
    void fadeAllCues();
    
    //AudioQueFunctions==================================
    AudioQuePlayerUI* newAudioQue();
    void incArraySize(AudioQuePlayerUI* newAudioQuePlayerUI);
    void removeAudioQue();
    AudioQuePlayerUI* get (int index);
    int size();
    
private:
    int queIndex;
    int playbackIndex;
    int audioQuePlayerUIArrayIndex;
    
    AudioQuePlayerUI* audioQuePlayerUIArray;
    Audio& audio;
    
};

#endif /* QueController_hpp */
>>>>>>> 2f31a3a80b54b13e7ab776be52b9930cc19897c3
