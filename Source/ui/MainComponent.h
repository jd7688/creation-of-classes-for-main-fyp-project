/*
  ==============================================================================
    This file was auto-generated!
  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../ui/AudioQuePlayerUI.h"
#include "../quecontroller/QueController.hpp"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
						public ButtonListener
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    void resized() override;

	//ButtonCode===================================================================

    void intButtons();
	void buttonClicked(Button* button) override;
	void fadeButtonClicked();
	void goButtonClicked();
	void stopButtonClicked();

    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
		CueMenu,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
		Exit = 2,
        
        NumFileItems
    };

	enum CueMenuItems
	{
		NewAudioCue = 1,
		NewControlCue = 2,

		NumCueItems
	};

    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

private:
    Audio& audio;
//	AudioQuePlayerUI audioQuePlayerUI;
//	AudioQuePlayer audioQuePlayer;
	TextButton fadeButton;
	TextButton goButton;
	TextButton stopButton;
    ScopedPointer<QueController> queController;
    
	int queIndex = 0;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED