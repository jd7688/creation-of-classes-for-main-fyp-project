/*=================================================================
AudioQuePlayerUI.cpp
=================================================================*/

#include "AudioQuePlayerUI.h"
//#include "../audio/Audio.h"

//=================================================================
//MainStuff

AudioQuePlayerUI::AudioQuePlayerUI()
{
	previewButton.setButtonText(">");
	previewButton.addListener(this);
	previewButton.setEnabled(false);
	addAndMakeVisible(&previewButton);

	AudioFormatManager formatManager;
	formatManager.registerBasicFormats();
	fileChooser = new FilenameComponent("audiofile",
		File::nonexistent,
		true, false, false,
		formatManager.getWildcardForAllFormats(),
		String::empty,
		"(choose a WAV or AIFF file)");
	fileChooser->addListener(this);
	addAndMakeVisible(fileChooser);
}

AudioQuePlayerUI::~AudioQuePlayerUI()
{
	delete fileChooser;
    previewButton.removeListener(this);
}

void AudioQuePlayerUI::resized()
{
	previewButton.setBounds(0, 0, getHeight(), getHeight());
	fileChooser->setBounds(getHeight(), 0, getWidth() - getHeight(), getHeight());
}

void AudioQuePlayerUI::buttonClicked(Button* button)
{
	if (button == &previewButton)
	{
		audioQuePlayer.setPlaying(!audioQuePlayer.isPlaying());
	}
}

void AudioQuePlayerUI::filenameComponentChanged(FilenameComponent* fileComponentThatHasChanged)
{
	if (fileComponentThatHasChanged == fileChooser)
	{
		File audioFile(fileChooser->getCurrentFile().getFullPathName());

		if (audioFile.existsAsFile())
		{
			audioQuePlayer.loadFile(audioFile);
		}

		else
		{
			AlertWindow::showMessageBox(AlertWindow::WarningIcon,
				"sdaTransport",
				"Couldn't open file!\n\n");
		}
	}
}