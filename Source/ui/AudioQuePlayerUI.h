/*=================================================================
AudioQuePlayerUI.h
=================================================================*/

#ifndef AUDIOQUEPLAYERUI_H_INCLUDED
#define AUDIOQUEPLAYERUI_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/AudioQuePlayer.h"

class AudioQuePlayerUI : public Component,
						 public Button::Listener,
						 public FilenameComponentListener
{
public:
	//=================================================================
	//MainStuff

	AudioQuePlayerUI();
	
	~AudioQuePlayerUI();

	void resized();

	void buttonClicked(Button* button);

	void filenameComponentChanged(FilenameComponent* fileComponentThatHasChanged);

	TextButton& getPreviewButton() { return previewButton; }

	//void setAudioQuePlayer(AudioQuePlayer* tAudioQuePlayer) { audioQuePlayer = tAudioQuePlayer; }

	//AudioQuePlayer& getAudioQuePlayer() {return &audioQuePlayer;}

private:
	TextButton previewButton;
	FilenameComponent* fileChooser;
    
	AudioQuePlayer audioQuePlayer;

};

#endif