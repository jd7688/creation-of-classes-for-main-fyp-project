/*
  ==============================================================================
    This file was auto-generated!
  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)//, audioQuePlayerUI(audio_.getAudioQuePlayer())
{
    setSize (500, 400);
    
    queController = new QueController (audio);
    
    intButtons();   //initiate Buttons
}

MainComponent::~MainComponent()
{
	//Remove listeners on closing of window.
	goButton.removeListener(this);
	stopButton.removeListener(this);
	fadeButton.removeListener(this);
}

void MainComponent::resized()
{
	//audioQuePlayerUI->setBounds(0, 0, getWidth(), 20);
}

//ButtonCode====================================================================

void MainComponent::intButtons()
{
    goButton.setBounds(10, 5, 60, 60);
    stopButton.setBounds(10, 70, 60, 60);
    fadeButton.setBounds(10, 135, 60, 60);
    
    goButton.setColour (TextButton::buttonColourId, Colours::green);
    stopButton.setColour (TextButton::buttonColourId, Colours::red);
    fadeButton.setColour (TextButton::buttonColourId, Colours::blue);
    
    goButton.setButtonText("Go");
    stopButton.setButtonText("Stop");
    fadeButton.setButtonText("Fade");
    
    addAndMakeVisible(goButton);
    addAndMakeVisible(stopButton);
    addAndMakeVisible(fadeButton);
    
    goButton.addListener(this);
    stopButton.addListener(this);
    fadeButton.addListener(this);
}

void MainComponent::buttonClicked(Button* button)
{
	//Button clicked function.
	if (button == &fadeButton)
		fadeButtonClicked();
	if (button == &goButton)
		goButtonClicked();
	if (button == &stopButton)
		stopButtonClicked();
}

//ButtonActions=================================================================
void MainComponent::fadeButtonClicked()
{
	DBG("Fade");
    queController->fadeAllCues();
}

void MainComponent::goButtonClicked()
{
	DBG("Go");
    queController->playNextCue();
}

void MainComponent::stopButtonClicked()
{
	DBG("Stop");
    queController->stopAllCues();
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", "Cues", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
	if (topLevelMenuIndex == 0)
	{
		menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
		menu.addItem(Exit, "Quit", true, false);
	}
	if (topLevelMenuIndex == 1)
	{
		menu.addItem(NewAudioCue, "New Audio Cue", true, false);
		menu.addItem(NewControlCue, "New Control Cue", true, false);
	}
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }

		else if (menuItemID == Exit)
		{
			abort();
		}
    }

	else if (topLevelMenuIndex == CueMenu)
	{
		if (menuItemID == NewAudioCue)
		{
            AudioQuePlayerUI *pAudioQuePlayerUI = queController->newAudioQue();
            addAndMakeVisible(pAudioQuePlayerUI);
		}

		else if (menuItemID == NewControlCue)
		{

		}
	}
}