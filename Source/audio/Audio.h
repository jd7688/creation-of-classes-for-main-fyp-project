/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#define audioPlayers 8

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "AudioQuePlayer.h"
#include "../ui/AudioQuePlayerUI.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback, 
				public Component
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */

	AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    //AudioMixerFunctions=======================================================
    AudioQuePlayer* getAvalibleAudioQuePlayer();
    
    void addAudioSource(AudioQuePlayer* newAudioSource);

private:
	AudioDeviceManager audioDeviceManager;
	MixerAudioSource audioSourceMixer;
	AudioSourcePlayer audioSourcePlayer;

	//AudioQuePlayers==============
	AudioQuePlayer audioQuePlayer[audioPlayers];
};

#endif  // AUDIO_H_INCLUDED