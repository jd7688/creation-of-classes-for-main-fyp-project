/*=================================================================
AudioQuePlayer.cpp
=================================================================*/

#include "AudioQuePlayer.h"

//=================================================================
//MainStuff
AudioQuePlayer::AudioQuePlayer()	:	thread ("FilePlayThread")
{
	thread.startThread();
	currentAudioFileSource = NULL;
}

AudioQuePlayer::~AudioQuePlayer()
{
	audioTransportSource.setSource(0);
	deleteAndZero(currentAudioFileSource);

	thread.stopThread(100);
}

void AudioQuePlayer::setPlaying(const bool newState)
{
	if (newState == true)
	{
		audioTransportSource.setPosition(0.0);
		audioTransportSource.start();
	}
	else
	{
		audioTransportSource.stop();
	}
}

bool AudioQuePlayer::isPlaying() const
{
	return audioTransportSource.isPlaying();
}

void AudioQuePlayer::setAudioPlayerAvalible(bool newState)
{
	audioDeviceAvalible = newState;
}

bool AudioQuePlayer::audioPlayerAvalible()
{
	return audioDeviceAvalible;
}

void AudioQuePlayer::loadFile(const File& newFile)
{
	setPlaying(false);
	audioTransportSource.setSource(0);
	deleteAndZero(currentAudioFileSource);

	AudioFormatManager formatManager;
	formatManager.registerBasicFormats();

	AudioFormatReader* reader = formatManager.createReaderFor(newFile);

	if (reader != 0)
	{
		currentAudioFileSource = new AudioFormatReaderSource(reader, true);

		audioTransportSource.setSource(currentAudioFileSource, 32768, 
										&thread, reader->sampleRate);

	}
}

void AudioQuePlayer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
	audioTransportSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void AudioQuePlayer::releaseResources()
{
	audioTransportSource.releaseResources();
}

void AudioQuePlayer::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill)
{
	audioTransportSource.getNextAudioBlock(bufferToFill);
}