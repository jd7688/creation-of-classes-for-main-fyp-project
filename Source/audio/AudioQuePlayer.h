/*====================================================================
AudioQuePlayer.h
====================================================================*/

#ifndef AUDIOQUEPLAYER_H_INCLUDED
#define AUDIOQUEPLAYER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

class AudioQuePlayer	: 	public AudioSource
							//public ChangeListener
{
public:
	//=================================================================
	//MainStuff
	AudioQuePlayer();
	~AudioQuePlayer();

	//====================================================================
	//Audio
	void setPlaying(const bool newState);

	bool isPlaying() const;

	void setAudioPlayerAvalible(bool newState);

	bool audioPlayerAvalible();

	void loadFile(const File& newFile);

	void prepareToPlay(int samplesPerBlockExpected, double sampleRate);
	void releaseResources();
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill);

private:
	AudioFormatReaderSource* currentAudioFileSource;
	AudioTransportSource audioTransportSource;
	bool audioDeviceAvalible = true;

	TimeSliceThread thread;
};

#endif