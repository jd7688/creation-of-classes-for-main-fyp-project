/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs

	//audioSourceMixer.addInputSource(&audioQuePlayer, true);
	audioSourcePlayer.setSource(&audioSourceMixer);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);

	audioSourceMixer.removeAllInputs();
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
	// get the audio from our file player - player puts samples in the output buffer
	audioSourcePlayer.audioDeviceIOCallback(inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);

    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
       //*outL = *inL;
       // *outR = *inR;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
	audioSourcePlayer.audioDeviceAboutToStart(device);
}

void Audio::audioDeviceStopped()
{
	audioSourcePlayer.audioDeviceStopped();
}

//AudioMixerFunctions=======================================================
AudioQuePlayer* Audio::getAvalibleAudioQuePlayer()
{
    DBG("getAvalibleAudioQuePlayer");
    return NULL;
}

void Audio::addAudioSource(AudioQuePlayer* newAudioSource)
{
    DBG("addAudioSource");
    audioSourceMixer.addInputSource(newAudioSource, true);
}












